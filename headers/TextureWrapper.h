/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.000
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    24/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Initial Creation
|
| Project: lute: Jibun 自分
|
| Description:
|   A "wrapper" that bundles up a bunch of
|	textures that are all to be used by
|	the same object
|
+-------------------------------------------
*/

#ifndef LUTE_TEXTUREWRAPPER_H
#define LUTE_TEXTUREWRAPPER_H

#include <vector>

#include <flags.h>

class TextureWrapper {		//TODO// I'm pretty sure I need to dealocate memory at some point in this thing...
	private:

	struct TextureUnit {
		char* source;
		int type; //as a flag
		int layer; //as a flag
	};

	std::vector<TextureUnit*> *texVec;

	public:
	//------------
	// Behaviour
	//------------
	TextureWrapper() { texVec = new std::vector<TextureUnit*>(3); }

	void add(char* source, int layer, int type);
	void removeTexture(char* source);
	void removeTexture(int layer);

	//----------
	// Getters
	//----------
	char* getSource(int layer);
	int getType(int layer);

};

#endif
