/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.000
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    18/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Original Build
|
| Project: lute: Jibun 自分
|
| Description:
|   contains all the flag integers for various
|	functions around the place
|
+-------------------------------------------
*/

#ifndef LD28_FLAGS_H
#define LD28_FLAGS_H

//Game constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int TILE_SIZE = 32;
const int FRAME_RATE = 60;

//Directions
const int JIBUN_NORTH = 200;
const int JIBUN_UP = 200;
const int JIBUN_EAST = 201;
const int JIBUN_RIGHT = 201;
const int JIBUN_SOUTH = 202;
const int JIBUN_DOWN = 202;
const int JIBUN_WEST = 203;
const int JIBUN_LEFT = 203;

const int JIBUN_NORTH_EAST = 210;
const int JIBUN_SOUTH_EAST = 211;
const int JIBUN_SOUTH_WEST = 212;
const int JIBUN_NORTH_WEST = 213;
const int JIBUN_TOP_RIGHT = 210;
const int JIBUN_BOTTOM_RIGHT = 211;
const int JIBUN_BOTTOM_LEFT = 212;
const int JIBUN_TOP_LEFT = 213;

const int JIBUN_HORIZONTAL = 220;
const int JIBUN_VERTICAL = 221;
const int JIBUN_BOTH_AXES = 222;
const int JIBUN_NO_AXES = 223;

//Identifiers
const int JIBUN_MOB = 300;
const int JIBUN_LEVEL = 301;
const int JIBUN_MANAGER = 302;

//Draw types
const int JIBUN_SOLID_COLOR = 100;
const int JIBUN_SINGLE_TILE = 101;
const int JIBUN_PADDED_TILE = 102;
const int JIBUN_REDUCED_TILE = 103;

//Layer types
const int JIBUN_SOLID = 200;
const int JIBUN_LADDER = 201;
const int JIBUN_ONE_WAY_FLOOR = 202;
const int JIBUN_MASK = 210;

const int JIBUN_SOLID_NODE = 220;
const int JIBUN_LADDER_NODE = 221;
const int JIBUN_ONE_WAY_NODE = 222;

const int JIBUN_ANY = 500; //NOT FULLY IMPLEMENTED

//Misc
const int JIBUN_ANTILAYER = 1000;
#endif
