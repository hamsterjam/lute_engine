/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.030
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    18/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Moved Flags to a flags.h
|
| Project: lute: Jibun 自分
|
| Description:
|   I dont even know if this is going to
|	cover more than the player but better
|	safe than sorry tbh.
|
+-------------------------------------------
*/

#ifndef LD28_MOB_H
#define LD28_MOB_H

#include <allegro5/allegro.h>

#include <Unit.h>
#include <Manager.h>
#include <flags.h>

class Manager; //FIXME// Its doing this shit again

const float MOVE_ACCELERATION = 6*TILE_SIZE;
const float GRAVITY_ACCELERATION = 30*TILE_SIZE;
const float JUMP_ACCELERATION = 10.5*TILE_SIZE;
const float LEDGE_PUSH_ACCELERATION = 3*TILE_SIZE;
const float SPEED_CAP_X = 8*TILE_SIZE;
const float SPEED_CAP_Y = 100*TILE_SIZE;
const float FRICTION_COEFFICENT = 0.5;

class Mob: public Unit {
	private:

	int x, y;		//In pixels, should be the CENTER of the thing
	float fx, fy; 	//Should be kept parallel to x and y
	float vx, vy;	//In pixels per second
	int width;
	int height;

	int ledgepush;
	bool ladderGrab;

	char* texPath;
	ALLEGRO_BITMAP *tex;

	bool NPC;		//Does this even do anything?

	public:
	//------------
	// Behaviour
	//------------
	int getIdentifier() {return JIBUN_MOB;}

	Mob(int x, int y, char* texPath, int width, int height, bool NPC);
	~Mob() { al_destroy_bitmap(tex); }

	bool init();
	void update(float delta);
	void draw(int dx, int dy, float scale);

	void move(int Direction);
	void jump();

	bool isClipping(int Direction, int layer, int xOff, int yOff);
	bool isClipping(int Direction, int layer) { return isClipping(Direction, layer, 0, 0); }
	bool isClipping(int Direction) {return isClipping(Direction, JIBUN_SOLID); }

	bool overTile(int layer);

	bool onGround(int layer);
	bool onGround() {return onGround(JIBUN_SOLID); }

	bool onCeiling(int layer);
	bool onCeiling() {return onCeiling(JIBUN_SOLID); }

	bool onWall(int direction, int layer);
	bool onWall(int direction) {return onWall(direction, JIBUN_SOLID);}

	//----------
	// Setters
	//----------
	void grabLadder(bool ladderGrab);

	//----------
	// Getters
	//----------
	int getX() {return x;}
	int getY() {return y;}

	int getWidth() {return width;}
	int getHeight() {return height;}

	bool onLadder() {return ladderGrab; }

};


#endif
