/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.110
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    18/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Moved Flags to a flags.h
|
| Project: lute: Jibun 自分
|
| Description:
|   A level of tiles, as youd expect
|
+-------------------------------------------
*/

#ifndef LD28_LEVEL_H
#define LD28_LEVEL_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include <vector>

#include <Unit.h>
#include <Manager.h>
#include <TextureWrapper.h>
#include <flags.h>

class Manager; //TODO// Its doing that thing again

const float JUMP_CURVE_MAX_X = 65.5650004903075;

class Level: public Unit {
	private:

	int width, height; //In # of tiles
	int tileSize;

	std::vector<std::vector<bool> >* solidArray;
	std::vector<std::vector<bool> >* ladderArray;
	std::vector<std::vector<bool> >* oneWayFloorArray;

	char* maskPath;
	TextureWrapper* textures;
	ALLEGRO_COLOR color;
	ALLEGRO_BITMAP *tex;

	ALLEGRO_BITMAP *getRoundingTile(int type, int category, int position, ALLEGRO_BITMAP *tileSet);
	bool drawLayer(int layer, ALLEGRO_BITMAP* tex);
	bool genTex();
	std::vector<std::vector<bool> > *getLayerArray(int layer);

	public:
	//------------
	// Behaviour
	//------------
	int getIdentifier() {return JIBUN_LEVEL;}

	Level(TextureWrapper* textures, ALLEGRO_COLOR color, unsigned int tileSize);	//TODO//Allow multiple masks somehow
	~Level() { al_destroy_bitmap(tex); }

	bool init();
	void update(float delta);
	void draw(int dx, int dy, float scale);

	//----------
	// Setters
	//----------
	void setTileAt(int x, int y, bool value, int layer);
	void setTileAt(int x, int y, bool value) { setTileAt(x, y, value, JIBUN_SOLID); }

	//----------
	// Getters
	//----------
	bool isTileAt(int x, int y, int layer);
	bool isTileAt(int x, int y) { return isTileAt(x, y, JIBUN_SOLID); }
	int getWidth() {return width*tileSize;}
	int getHeight() {return height*tileSize;}
	int getTileSize() {return tileSize;}
};

#endif
