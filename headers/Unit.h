/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.000
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    14/12/13
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Original Creation
|
| Project: lute: Jibun 自分
|
| Description:
|   This is here for polymorphism
|
+-------------------------------------------
*/

#ifndef LD28_UNIT_H
#define LD28_UNIT_H

class Manager;

class Unit {
	private:
	bool debug;
	Manager* man;

	public:
	Unit() { debug = false; }

	virtual bool init() = 0;
	virtual void draw(int dx, int dy, float scale) = 0;
	virtual void update(float delta) = 0;

	void setDebug(bool debug) { this-> debug = debug; }
	void setManager(Manager* man) {this->man = man;}
	void toggleDebug() { debug = !debug; }

	bool isDebug() { return debug; }
	Manager* getManager() { return man; }

	virtual int getIdentifier() = 0;
};

#endif
