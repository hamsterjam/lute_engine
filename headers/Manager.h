/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.010
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    14/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Removed Build Layer
|
| Project: lute: Jibun 自分
|
| Description:
|   A comunication link between game objects
|
+-------------------------------------------
*/

#ifndef LD28_MANAGER_H
#define LD28_MANAGER_H

#include <vector>

#include <allegro5/allegro.h>

#include <Unit.h>
#include <Level.h>
#include <Mob.h>

class Level; //FIXME// Fix this bug, eh I guess It dosnt really matter
class Mob;

class Manager: public Unit {
	private:
	std::vector<Unit*> *objects;
	std::vector<Manager*>* subManagers;

	Level* map;
	Mob* player;

	int x, y;
	int width, height;
	float scale;

	ALLEGRO_DISPLAY* disp;

	bool keysDown[ALLEGRO_KEY_MAX];
	bool buttonsDown[4];

	bool keysPressed[ALLEGRO_KEY_MAX];
	bool buttonsPressed[4];

	public:
	//------------
	// Behaviour
	//------------
	int getIdentifier() {return JIBUN_MANAGER;}

	Manager(ALLEGRO_DISPLAY* disp, float scale, int x, int y, int width, int height);

	void handleMouseClick(int localX, int localY, int button);

	bool init();
	void draw(int dx, int dy, float scale);
	void update(float delta);

	void update();
	void resetPressed();
	
	//----------
	// Setters
	//----------
	void setLevel(Level* map);
	void setPlayer(Mob* player);

	void setPos(int x, int y) {this->x = x; this->y = y;}
	void setWidth(int width) {this->width = width;}
	void setHeight(int height) {this->height = height;}

	void addObject(Unit* obj);
	//Keyboard
	void keyPressed(int keycode);
	void keyReleased(int keycode) { keysDown[keycode] = false; }
	//Mouse
	void buttonPressed(int button);
	void buttonReleased(int button);

	//----------
	// Getters
	//----------
	std::vector<Unit*>* getObjects() { return objects; }
	ALLEGRO_DISPLAY* getDisplay() { return disp; }

	Level* getMap() { return map; }
	Mob* getPlayer() { return player; }

	bool isKeyDown(int keycode) { return keysDown[keycode]; }
	bool isButtonDown(int button) { return buttonsDown[button]; }

	bool isKeyPressed(int keycode) { return keysPressed[keycode]; }
	bool isButtonPressed(int button) { return buttonsPressed[button]; }

	int getX() { return x; }
	int getY() { return y; }
	int getWidth() { return width; }
	int getHeight() { return height; }

	int getCameraOffsetX(int screenWidth);
	int getCameraOffsetY(int screenHeight);

	int toWorldX(int screenX);
	int toWorldY(int screenY);
};

#endif
