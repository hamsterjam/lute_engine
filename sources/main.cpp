/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.010
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    14/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Removed Build Layer
|
| Project: lute: Jibun 自分
|
| Description:
|	Just the main method and "not much" else.
|
+-------------------------------------------
*/

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>

#include <iostream>
#include <vector>

#include <Unit.h>
#include <Level.h>
#include <Mob.h>
#include <TextureWrapper.h>
#include <flags.h>

using namespace std;
									//TODO// Go through EVERYTHING and look for mem leaks
ALLEGRO_DISPLAY* disp;
ALLEGRO_TIMER* timer;
ALLEGRO_EVENT_QUEUE* eq;

void endGame(char* message);

void allegroInit() {
	if (!al_init() ) {
		endGame("Failed to initialise Allegro.");
	}
	if (!al_init_image_addon() ) {
		endGame("Failed to initialise image addon.");
	}
	if (!al_init_primitives_addon() ){
		endGame("Failed to initialise primitives addon.");
	}
	if (!al_install_keyboard() ) {
		endGame("Failed to install keyboard.");
	}
	if (!al_install_mouse() ) {
		endGame("Failed to install mouse.");
	}
	timer = al_create_timer(1.0/FRAME_RATE);
	if (!timer) {
		endGame("Failed to create timer.");
	}
	disp = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);
	if (!disp) {
		endGame("Failed to create display.");
	}

	al_set_window_position(disp, 100, 100);

	eq = al_create_event_queue();
	if (!eq) {
		endGame("Failed to create event queue.");
	}

	al_register_event_source(eq, al_get_keyboard_event_source());
	al_register_event_source(eq, al_get_mouse_event_source());
	al_register_event_source(eq, al_get_timer_event_source(timer));
	al_register_event_source(eq, al_get_display_event_source(disp));
}

void allegroCleanup() {
	if (timer) {
		al_destroy_timer(timer);
	}
	if (disp) {
		al_destroy_display(disp);
	}
	if (eq) {
		al_destroy_event_queue(eq);
	}
}

void endGame(char* message) {
	cout << message << endl;
	allegroCleanup();
	exit(1);
}

//------------------------------
// Important stuff starts here
//------------------------------
Manager* man;

void userInit() {
	man = new Manager(disp, 1, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	//Wrap the textures
	TextureWrapper* levelTex = new TextureWrapper;
	levelTex->add("resources/testlevel.png", JIBUN_MASK, 0);
	levelTex->add("", JIBUN_SOLID, JIBUN_SOLID_COLOR);
	levelTex->add("resources/testladder.png", JIBUN_LADDER, JIBUN_REDUCED_TILE);
	levelTex->add("resources/testoneway.png", JIBUN_ONE_WAY_FLOOR, JIBUN_SINGLE_TILE);

	man->addObject(new Level(levelTex, al_map_rgb(127, 127, 127), TILE_SIZE));
	man->addObject(new Mob(48, 592 -8, "resources/testtile.png", TILE_SIZE-2, TILE_SIZE*1.5, false));
	man->addObject(new Mob(21*32 + 16, 14*32 + 16 - 8, "resources/testtile.png", TILE_SIZE-2, TILE_SIZE*1.5, true));

	//new Manager(man, 0.125, SCREEN_WIDTH - 160, 0, 160, 80);

	if(!man->init()) {
		endGame("Failed to initialise game items");
	}

	al_set_target_backbuffer(disp);
}

void userUpdate() {
	man->update(1.0/FRAME_RATE);

	man->resetPressed();
}

void userDraw() {
	man->draw(0, 0, 1);
}

void userCleanup() {
	delete man;
}

//----------------------------
// Important stuff ends here
//----------------------------

int main(int argc, char** argv) {
	allegroInit();

	userInit();

	bool redraw = true;
	bool exit = false;
	al_start_timer(timer);

	while (!exit) {
		ALLEGRO_EVENT e;
		al_wait_for_event(eq, &e);

		if (e.type == ALLEGRO_EVENT_TIMER) {
			redraw = true;
			userUpdate();
		}
		//Keyboard
		else if (e.type == ALLEGRO_EVENT_KEY_DOWN) {
			if (e.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
				exit  = true;
			}
			man->keyPressed(e.keyboard.keycode);
		}
		else if (e.type == ALLEGRO_EVENT_KEY_UP) {
			man->keyReleased(e.keyboard.keycode);
		}
		//Mouse
		else if (e.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			man->buttonPressed(e.mouse.button);
		}
		else if (e.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
			man->buttonReleased(e.mouse.button);
		}

		else if (e.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			exit = true;
		}

		if (redraw == true && al_is_event_queue_empty(eq) ) {
			redraw = false;
			al_set_target_backbuffer(disp);
			al_clear_to_color(al_map_rgb(255,255,255));
			userDraw();
			al_flip_display();
	}	}

	allegroCleanup();
	userCleanup();
	return 0;
}
