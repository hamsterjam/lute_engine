/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.020
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    17/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Added a seperate jump function
|
| Project: lute: Jibun 自分
|
| Description:
|   I dont even know if this is going to
|	cover more than the player but better
|	safe than sorry tbh.
|
+-------------------------------------------
*/

#include <iostream>
#include <cmath>

#include <allegro5/allegro_primitives.h>

#include <Mob.h>

//TODO//Seperate keyboard movement from movement controlled by some NPC controller

using namespace std;

Mob::Mob(int x, int y, char* texPath, int width, int height, bool NPC) {
	this->x = x;
	this->y = y;
	this->texPath = texPath;
	this->width = width;
	this->height = height;
	this->NPC = NPC;

	ledgepush = 0;

	fx = (float) x;
	fy = (float) y;
	vx = 0;
	vy = 0;
}

bool Mob::init() {
	tex = al_load_bitmap(texPath);
	if (!tex) {
		return false;
	}
	fx = (float) x;
	fy = (float) y;

	if (!NPC) {
		getManager()->setPlayer(this);
	}

	return true;
}

void Mob::update(float delta) {
	Manager* man = getManager();

	int tileSize = man->getMap()->getTileSize();

	int tileX = x/tileSize;
	int tileY = y/tileSize;

	if (NPC) {
		//If we are dangling off a ledge, correct
		if (onGround(JIBUN_ANY)) {
			int leftEdge = x - width/2;
			int rightEdge = x + width/2;
			int bellow = y + height/2;

			if (!man->getMap()->isTileAt(rightEdge/tileSize, bellow/tileSize, JIBUN_ANY)) {
				//Over right edge
				move(JIBUN_LEFT);
			}
			else if (!man->getMap()->isTileAt(leftEdge/tileSize, bellow/tileSize, JIBUN_ANY)) {
				//Over left edge
				move(JIBUN_RIGHT);
	}	}	}

	//Gravity em up (but only if not on a ladder)
	if (!onLadder()) vy += GRAVITY_ACCELERATION * delta;

	//Rudimentary as fuck friction	//TODO//Replace this garbage
	vx -= vx*FRICTION_COEFFICENT;
	if (onLadder()) vy -= vy*FRICTION_COEFFICENT;

	//Check if our speed is over the max, if so, scale it
	if (vx > SPEED_CAP_X) vx = SPEED_CAP_X;
	if (vx < -SPEED_CAP_X) vx = -SPEED_CAP_X;
	if (vy > SPEED_CAP_Y) vy = SPEED_CAP_Y;
	if (vy < -SPEED_CAP_Y) vy = -SPEED_CAP_Y;

	//Check if we are at the top of a ladder
	//if (vy < 0 && !man->getMap()->isTileAt(x/tileSize, (y - height/4)/tileSize, JIBUN_LADDER) && onLadder()) vy = 0;

	//Make sure that moving will not clip us through an object
	//If the move will make us clip, move adjacent and set velocity to 0
	if (isClipping(JIBUN_LEFT, JIBUN_SOLID, (int) (vx*delta), (int) (vy*delta))) {
		int potX = ((x - width/2 + tileSize/2)/tileSize)*tileSize + width/2;
		if (potX <= x + 1 && vx < 0) {	//+1 to account for floating point arithmetic fucking me over
			vx = 0;
			fx = potX;
	}	}
	else if (ledgepush > 0) ledgepush = 0;

	if (isClipping(JIBUN_RIGHT, JIBUN_SOLID, (int) (vx*delta), (int) (vy*delta))) {		//The logic behind that potX is long. Thats all I really have to say on the matter.
		int potX = ((x + width/2 - tileSize/2)/tileSize + 1)*tileSize - width/2;		//TODO// Replace this with something nicer
		if (potX >= x && vx > 0) {
			vx = 0;
			fx = potX;
	}	}
	else if (ledgepush < 0) ledgepush = 0;

	if (isClipping(JIBUN_UP, JIBUN_SOLID, (int) (vx*delta), (int) (vy*delta))) {
		int potY =  ((y - height/2 + tileSize/2)/tileSize)*tileSize + height/2;
		if (potY <= y + 1) {
			vy = 0;
			fy = potY;
	}	}

	if (isClipping(JIBUN_DOWN, JIBUN_SOLID, (int) (vx*delta), (int) (vy*delta))) {
		if (!man->getMap()->isTileAt(x/tileSize, tileY + 1, JIBUN_ANY) && (int) vy >= 0) {	//No tile bellow middle
			int left = (x + width/2 + 1)/tileSize;
			int right =  (x - width/2)/tileSize;
			int lower = (y + height/2)/tileSize;

			if (man->getMap()->isTileAt(left, lower, JIBUN_ANY)) {
				//Drop left (push left)
				ledgepush = -1;
				if (vx > 0) vx = 0;
			}
			else if (man->getMap()->isTileAt(right, lower, JIBUN_ANY)) {
				//Drop right (push right)
				ledgepush = 1;
				if (vx < 0) vx = 0;
		}	}

		else {
			int potY = ((y + height/2 - tileSize/2)/tileSize + 1)*tileSize - height/2;
			if (potY >= y) {
				vy = 0;
				fy = potY;
	}	}	}

	//Also check collisions for one-way floors
	if (isClipping(JIBUN_DOWN, JIBUN_ONE_WAY_FLOOR, (int) (vx*delta), (int) (vy*delta)) && !isClipping(JIBUN_DOWN, JIBUN_ONE_WAY_FLOOR, 0, -1) && vy >= 0) {
		if (!man->getMap()->isTileAt(x/tileSize, tileY + 1, JIBUN_ANY) && (int) vy >= 0) {	//No tile bellow middle
			int left = (x + width/2 + 1)/tileSize;
			int right =  (x - width/2)/tileSize;
			int lower = (y + height/2)/tileSize;

			if (man->getMap()->isTileAt(left, lower, JIBUN_ANY)) {
				//Drop left (push left)
				ledgepush = -1;
				if (vx > 0) vx = 0;
			}
			else if (man->getMap()->isTileAt(right, lower, JIBUN_ANY)) {
				//Drop right (push right)
				ledgepush = 1;
				if (vx < 0) vx = 0;
		}	}

		else {
			int potY = ((y + height/2 - tileSize/2)/tileSize + 1)*tileSize - height/2;
			if (potY >= y) {			//We also need that this will only ever put us on TOP of a tile
				vy = 0;					//Collisions atm assume u will never clip, here we clip on purpose
				fy = potY;
	}	}	}

	//Same thing for ladders
	if (isClipping(JIBUN_DOWN, JIBUN_LADDER, (int) (vx*delta), (int) (vy*delta)) && !isClipping(JIBUN_DOWN, JIBUN_LADDER, 0, -1)
				&& vy >= 0 && !overTile(JIBUN_LADDER)) {// !man->getMap()->isTileAt(tileX, (y+height/2)/tileSize, JIBUN_LADDER)) {

		if (!man->getMap()->isTileAt(x/tileSize, tileY + 1, JIBUN_ANY) && (int) vy >= 0) {	//No tile bellow middle
			int left = (x + width/2 + 1)/tileSize;
			int right =  (x - width/2)/tileSize;
			int lower = (y + height/2)/tileSize;

			if (man->getMap()->isTileAt(left, lower, JIBUN_ANY)) {
				//Drop left (push left)
				ledgepush = -1;
				if (vx > 0) vx = 0;
			}
			else if (man->getMap()->isTileAt(right, lower, JIBUN_ANY)) {
				//Drop right (push right)
				ledgepush = 1;
				if (vx < 0) vx = 0;
		}	}

		else {
			int potY = ((y + height/2 - tileSize/2)/tileSize + 1)*tileSize - height/2;
			if (potY >= y) {			//We also need that this will only ever put us on TOP of a tile
				vy = 0;					//Collisions atm assume u will never clip, here we clip on purpose
				fy = potY;
	}	}	}


	//Push if we intentionaly clip the wall for dropping off a ledge
	vx += ledgepush*LEDGE_PUSH_ACCELERATION;

	//Move
	fx += vx * delta;
	x = (int) fx;

	fy += vy * delta;
	y = (int) fy;

	//If after all this, your no longer on a ladder, you should drop
	if (!man->getMap()->isTileAt(tileX, (y+height/2)/tileSize, JIBUN_LADDER) && !overTile(JIBUN_LADDER) ) grabLadder(false);
}

void Mob::draw(int dx, int dy, float scale) {	//TODO// Proper texture support for mobs
	Manager* man = getManager();

	float x = this->x*scale;
	float y = this->y*scale;

	float width = this->width*scale;
	float height = this->height*scale;

	al_draw_filled_rectangle(x - width/2 - dx, y - height/2 - dy, x + width/2 - dx, y + height/2 - dy, al_map_rgb(127,0,0));
	if (isDebug()) {
		al_draw_rectangle(x - width/2 - dx + 1*scale, y - height/2 - dy + 1*scale, x + width/2 - dx - 1*scale, y + height/2 - dy - 1*scale, al_map_rgb(255, 0 ,0), 2*scale);
		al_draw_line(x - dx, y - height/2 - dy, x - dx, y + height/2 - dy, al_map_rgb(255, 0, 0), 2*scale);
	}
}

bool Mob::isClipping(int direction, int layer, int xOff, int yOff) {
	Manager* man = getManager();
	int s = man->getMap()->getTileSize();
	int w = width;
	int h = height;
	int x = this->x + xOff;
	int y = this->y + yOff;

	switch (direction) {		//PREPARE FOR A BUMPY RIDE
		case JIBUN_UP:
			for (int i = x - w/2 +1; i < x + w/2 - 1; i += s) {
				if (man->getMap()->isTileAt((i)/s, (y - h/2)/s, layer) && y - h/2 <= s*(y/s)) return true;
			}
			if (man->getMap()->isTileAt((x + w/2 - 1)/s, (y - h/2)/s, layer) && y - h/2 <= s*(y/s)) return true;
			return false;
		case JIBUN_DOWN:
			for (int i = x - w/2 +1; i < x + w/2 - 1; i += s) {
				if (man->getMap()->isTileAt((i)/s, (y + h/2)/s, layer) && y + h/2 >= s*(y/s + 1)) return true;
			}
			if (man->getMap()->isTileAt((x + w/2 - 1)/s, (y + h/2)/s, layer) && y + h/2 >= s*(y/s + 1)) return true;
			return false;
		case JIBUN_LEFT:
			for (int i = y - h/2 + 1; i < y + h/2 - 1; i += s) {
				if (man->getMap()->isTileAt((x - w/2)/s, (i)/s, layer) && x - w/2 <= s*(x/s)) return true;
			}
			if (man->getMap()->isTileAt((x - w/2)/s, (y + h/2 - 1)/s, layer) && x - w/2 <= s*(x/s)) return true;
			return false;
		case JIBUN_RIGHT:
			for (int i = y - h/2 + 1; i < y + h/2 - 1; i += s) {
				if (man->getMap()->isTileAt((x + w/2)/s, (i)/s, layer) && x + w/2 >= s*(x/s + 1)) return true;
			}
			if (man->getMap()->isTileAt((x + w/2)/s, (y + h/2 - 1)/s, layer) && x + w/2 >= s*(x/s + 1)) return true;
			return false;
	}
	return false;
}

void Mob::move(int direction) {
	Manager* man = getManager();
	int tileSize = man->getMap()->getTileSize();
	switch (direction) {
		case JIBUN_UP:
			//If your OVER a ladder, grab it and climb
			if (man->getMap()->isTileAt(x/tileSize, (y+height/2)/tileSize, JIBUN_LADDER) || overTile(JIBUN_LADDER)) {
				vy -= MOVE_ACCELERATION;							//TODO// This all needs to be multiplied by delta. How did I fuck that up?
				grabLadder(true);
			}
			break;
		case JIBUN_RIGHT:
			//Walk
			if (ledgepush >= 0) vx += MOVE_ACCELERATION;
			break;
		case JIBUN_DOWN:
			//If your on a ladder, climb it
			if(onLadder()) vy += MOVE_ACCELERATION;

			//If your standing atop a ladder
			if(onGround(JIBUN_LADDER) && !onGround()) {
				y++ ;
				fy++ ;
				grabLadder(true);
			}

			//If your on a one way floor BUT NOT A REGULAR FLOOR, clip it on purpose
			if(onGround(JIBUN_ONE_WAY_FLOOR) && !onGround()) {
				y++ ;
				fy++ ;
			}
			break;
		case JIBUN_LEFT:
			//Walk
			if (ledgepush <= 0) vx -= MOVE_ACCELERATION;
			break;
	}
}

void Mob::jump() {
	//Jump
	if (((onGround() || onGround(JIBUN_ONE_WAY_FLOOR) || onGround(JIBUN_LADDER)) && vy == 0) || (onLadder() && (int) vy >= 0) ) vy -= JUMP_ACCELERATION;
	//You also want to release a ladder with this
	grabLadder(false);
}

bool Mob::overTile(int layer) {
	Manager* man = getManager();
	int tileSize = man->getMap()->getTileSize();
	return (man->getMap()->isTileAt(x/tileSize, y/tileSize, layer) );
}

bool Mob::onGround(int layer) {
	if (layer == JIBUN_ANY) {
		return onGround() || onGround(JIBUN_ONE_WAY_FLOOR) || onGround(JIBUN_LADDER);
	}
	return (isClipping(JIBUN_DOWN, layer, 0, 0) && !isClipping(JIBUN_DOWN, layer, 0, -1));
}

bool Mob::onCeiling(int layer) {
	return (isClipping(JIBUN_UP, layer, 0, -1) && !isClipping(JIBUN_UP, layer, 0, 0));
}

bool Mob::onWall(int direction, int layer) {
	switch (direction) {
		case JIBUN_RIGHT:
			return (isClipping(direction, layer, 0, 0) && !isClipping(direction, layer, -1, 0));

		case JIBUN_LEFT:
			return (isClipping(direction, layer, -1, 0) && !isClipping(direction, layer, 0, 0));
	}
}

void Mob::grabLadder(bool ladderGrab) {
	if(!this->ladderGrab && ladderGrab) vy = 0;
	this->ladderGrab = ladderGrab;
}
