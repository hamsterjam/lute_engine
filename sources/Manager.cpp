/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.020
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    17/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Added a jump key
|
| Project: lute: Jibun 自分
|
| Description:
|   A comunication link between game objects
|
+-------------------------------------------
*/

#include <allegro5/allegro.h>

#include <iostream>
#include <Manager.h>
#include <Mob.h>

using namespace std;

Manager::Manager(ALLEGRO_DISPLAY* disp, float scale, int x, int y, int width, int height) {
	this->disp = disp;
	this->scale = scale;
	this->x = x;
	this->y = y;

	this->height = height;
	this->width = width;

	objects = new std::vector<Unit*>(0);

	//Initalise the fucking key array to fucking zeros
	for(int i = 0; i < ALLEGRO_KEY_MAX; i++) {
		keysDown[i] = false;
		keysPressed[i] = false;
	}
	for (int i=0; i < 4; i++) {
		buttonsDown[i] = false;
		buttonsPressed[i] = false;
	}
}

void Manager::handleMouseClick(int localX, int localY, int button) {
	//The mouse, it does nothing!!
}

bool Manager::init() {
	for (vector<Unit*>::iterator it = objects->begin(); it != objects->end(); it++) {
		Unit* curr = *it;
		if (!curr->init()) {
			return false;
	}	}
	return true;
}

void Manager::addObject(Unit* obj) {
	objects->push_back(obj);
	obj->setManager(this);
}

void Manager::update(float delta) {
	//Poll Mouse coords
	ALLEGRO_MOUSE_STATE mouse;
	al_get_mouse_state(&mouse);

	//Player movement
	if (isKeyPressed(ALLEGRO_KEY_SPACE)) {
		player->jump();
	}
	if (isKeyDown(ALLEGRO_KEY_W)) {
		player->move(JIBUN_UP);
	}
	if (isKeyDown(ALLEGRO_KEY_D)) {
		player->move(JIBUN_RIGHT);
	}
	if (isKeyDown(ALLEGRO_KEY_A)) {		//TODO// Unmagic the keys (make them rebindable)
		player->move(JIBUN_LEFT);
	}
	if (isKeyDown(ALLEGRO_KEY_S)) {
		player->move(JIBUN_DOWN);
	}

	if (isKeyDown(ALLEGRO_KEY_LSHIFT) && isKeyPressed(ALLEGRO_KEY_F3)) {
		toggleDebug();
		for (vector<Unit*>::iterator it = objects->begin(); it != objects->end(); it++) {
			(*it)->toggleDebug();
	}	}

	// Mouse Handling
	handleMouseClick(mouse.x, mouse.y, 2);

	//Update the other game objects
	for (vector<Unit*>::iterator it = objects->begin(); it != objects->end(); it++) {
		Unit* curr = *it;
		curr->update(delta);
	}
}

void Manager::draw(int dx, int dy, float scale) {
	//Draw game objects
	int xOff = dx + getCameraOffsetX(width);
	int yOff = dy + getCameraOffsetY(height);
	float totScale = this->scale*scale;

	for (vector<Unit*>::iterator it = objects->begin(); it != objects->end(); it++) {
		Unit* curr = *it;
		curr->draw(xOff, yOff, totScale);
	}
}

void Manager::resetPressed() {
	for(int i = 0; i < ALLEGRO_KEY_MAX; i++) {
		keysPressed[i] = false;
	}
	for (int i=0; i < 4; i++) {
		buttonsPressed[i] = false;
	}
}

void Manager::setLevel(Level* map) {
	this->map = map;
}

void Manager::setPlayer(Mob* player) {
	this->player = player;
}

int Manager::getCameraOffsetX(int screenWidth) {
	int val = scale*player->getX() - screenWidth/2;

	if (scale*map->getWidth() <= screenWidth) return 0;

	if (val < 0) return 0;
	if (val > scale*map->getWidth() - screenWidth) return scale*map->getWidth() - screenWidth;
	return val;
}

int Manager::getCameraOffsetY(int screenHeight) {
	int val = scale*player->getY() - screenHeight/2;

	if (scale*map->getHeight() <= screenHeight) return 0;

	if (val < 0) return 0;
	if (val > scale*map->getHeight() - screenHeight) return scale*map->getHeight() - screenHeight;
	return val;
}

int Manager::toWorldX(int screenX) {
	return (screenX + getCameraOffsetX(width) - x)/scale;
}

int Manager::toWorldY(int screenY) {
	return (screenY + getCameraOffsetY(height) - y)/scale;
}

void Manager::keyPressed(int keycode) {
	if (!isKeyDown(keycode)) {
		keysPressed[keycode] = true;
	}
	keysDown[keycode] = true;
}

void Manager::buttonPressed(int button) {
	if (!isButtonDown(button)) {
		buttonsPressed[button] = true;
	}
	buttonsDown[button] = true;
}

void Manager::buttonReleased(int button) {
	buttonsDown[button] = false;
}
