/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.000
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    24/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Initial Creation
|
| Project: lute: Jibun 自分
|
| Description:
|   A "wrapper" that bundles up a bunch of
|	textures that are all to be used by
|	the same object
|
+-------------------------------------------
*/

#include <vector>
#include <iostream>

#include <TextureWrapper.h>

using namespace std;

void TextureWrapper::add(char* source, int layer, int type) {
	removeTexture(layer);
	TextureUnit* newTex = new TextureUnit;
	newTex->source = source;
	newTex->type = type;
	newTex->layer = layer;

	texVec->push_back(newTex);
}

void TextureWrapper::removeTexture(char* source) {
	for(vector<TextureUnit*>::iterator it = texVec->begin(); it != texVec->end(); it++) {
		if (*it && (*it)->source == source) {
			texVec->erase(it);
			break;
	}	}
}

void TextureWrapper::removeTexture(int layer) {
	for(vector<TextureUnit*>::iterator it = texVec->begin(); it != texVec->end(); it++) {
		if (*it && (*it)->layer == layer) {
			texVec->erase(it);
			break;
	}	}
}

char* TextureWrapper::getSource(int layer) {
	for(vector<TextureUnit*>::iterator it = texVec->begin(); it != texVec->end(); it++) {
		if (*it && (*it)->layer == layer) {
			return (*it)->source;
	}	}
	return 0;
}

int TextureWrapper::getType(int layer) {
	for(vector<TextureUnit*>::iterator it = texVec->begin(); it != texVec->end(); it++) {
		if (*it && (*it)->layer == layer) {
			return (*it)->type;
	}	}
	return -1;
}
