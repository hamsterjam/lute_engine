/*
+-------------------------------------------
|  ___        ___
| |\ __\     |\ __\  Version:
| | |  |     | |  |    1.100
| | |  |_____| |  |
| | |  |______\|  |  Last Updated:
| | |   __    _   |    14/05/14
| | |  || |  | |  |
|  \|__|| |  |\|__|  By:
|   ____| |  |         HamsterJam
|  \_____\|  |
|   \________/  Reason:
|                 Added ladders
|
| Project: lute: Jibun 自分
|
| Description:
|   A level of tiles, as youd expect
|
+-------------------------------------------
*/

#include <iostream>

#include <Level.h>

using namespace std;

ALLEGRO_BITMAP *Level::getRoundingTile(int type, int category, int position, ALLEGRO_BITMAP *tileSet) {

	if (type == JIBUN_REDUCED_TILE) {
		switch (position) {
			case 1:
				return al_create_sub_bitmap(tileSet, category * tileSize, 0, tileSize/2, tileSize/2);
			case 2:
				return al_create_sub_bitmap(tileSet, (2*category + 1) * tileSize/2, 0, tileSize/2, tileSize/2);
			case 3:
				return al_create_sub_bitmap(tileSet, (2*category + 1) * tileSize/2, tileSize/2, tileSize/2, tileSize/2);
			case 4:
				return al_create_sub_bitmap(tileSet, category * tileSize, tileSize/2, tileSize/2, tileSize/2);
			default:
				return NULL;
		}										// Trust me, theres logic behind this. This is essentially
	} else if (type == JIBUN_PADDED_TILE) {		// a hardcoding of the tile layout in the example tilesets.
		if (category == 4) {
			return al_create_sub_bitmap(tileSet, 3*tileSize, 0, tileSize, tileSize);
		}
		switch (position) {
			case 1:
				return al_create_sub_bitmap(tileSet, category * tileSize, 0, tileSize/2, tileSize/2);
			case 2:
				return al_create_sub_bitmap(tileSet, (2*category + 1)*tileSize/2, 0, tileSize/2, tileSize/2);
			case 3:
				return al_create_sub_bitmap(tileSet, (2*category + 1)*tileSize/2, tileSize/2, tileSize/2, tileSize/2);
			case 4:
				return al_create_sub_bitmap(tileSet, category * tileSize, tileSize/2, tileSize/2, tileSize/2);
			default:
				return NULL;
		}
	} else {
		cout << "Invalid genTex type." << endl;
	}
	return NULL;
}

bool Level::drawLayer(int layer, ALLEGRO_BITMAP* tex) {
	char* texturePath = textures->getSource(layer);
	int type = textures->getType(layer);
	if(!texturePath) { return true; } //This just means that layer isnt in the wrapper

	//This is fucking crazy code, do not recomend reading.
	if (type == JIBUN_REDUCED_TILE) {
		ALLEGRO_BITMAP *tileMap;
		tileMap = al_load_bitmap(texturePath);
		if (!tileMap) {
			cout << "Couldn't find: " << texturePath << endl;
			return false;
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (isTileAt(i, j, layer)) {

					//---------------------------
					// Top Left (position 1)
					//---------------------------

					int category = 0;

					if (!isTileAt(i-1, j, layer)) category += 1;
					if (!isTileAt(i-1, j-1, layer)) category += 2;
					if (!isTileAt(i, j-1, layer)) category += 4;

					al_draw_bitmap(getRoundingTile(type, category, 1, tileMap), i*tileSize, j*tileSize, 0);

					//---------------------------
					// Top Right (position 2)
					//---------------------------

					category = 0;

					if (!isTileAt(i, j-1, layer)) category += 1;
					if (!isTileAt(i+1, j-1, layer)) category += 2;
					if (!isTileAt(i+1, j, layer)) category += 4;

					al_draw_bitmap(getRoundingTile(type, category, 2, tileMap), (2*i + 1)*tileSize/2, j*tileSize, 0);

					//---------------------------
					// Bottom Right (position 3)
					//---------------------------

					category = 0;

					if (!isTileAt(i+1, j, layer)) category += 1;
					if (!isTileAt(i+1, j+1, layer)) category += 2;
					if (!isTileAt(i, j+1, layer)) category += 4;

					al_draw_bitmap(getRoundingTile(type, category, 3, tileMap), (2*i + 1)*tileSize/2, (2*j + 1)*tileSize/2, 0);

					//---------------------------
					// Bottom Left (positon 4)
					//---------------------------

					category = 0;

					if (!isTileAt(i, j+1, layer)) category += 1;
					if (!isTileAt(i-1, j+1, layer)) category += 2;
					if (!isTileAt(i-1, j, layer)) category += 4;

					al_draw_bitmap(getRoundingTile(type, category, 4, tileMap), i*tileSize, (2*j + 1)*tileSize/2, 0);
		}	}	}
		al_destroy_bitmap(tileMap);

	}

	else if (type == JIBUN_PADDED_TILE) {
		ALLEGRO_BITMAP *tileMap;
		tileMap = al_load_bitmap(texturePath);
		if (!tileMap) {
			cout << "Couldn't find: " << texturePath << endl;
			return false;
		}

		ALLEGRO_BITMAP *fullTile;
		fullTile = getRoundingTile(type, 4, 1, tileMap);

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (isTileAt(i, j, layer)) {
					// Draw the actual tile
					al_draw_bitmap(fullTile, i*tileSize, j*tileSize, 0);

					// Check the sides
					// Note that the corner pieces are deffered to top and bottom edges.

					// Starting with the left
					if (i > 0 && !isTileAt(i-1, j, layer)) {
						if (j == 0 || !isTileAt(i-1, j-1, layer)) { // If we are at the top or the cell above is empty
							al_draw_bitmap(getRoundingTile(type, 0, 4, tileMap), (2*i - 1)*tileSize/2, j*tileSize, 0);
						}
						if (j == height - 1 || !isTileAt(i-1, j+1, layer)) { //If we are at the bottom or the cell bellow is empty
							al_draw_bitmap(getRoundingTile(type, 0, 4, tileMap), (2*i - 1)*tileSize/2, (2*j + 1)*tileSize/2, 0);
					}	}
					// Now the right
					if (i < width - 1 && !isTileAt(i+1, j, layer)) {
						if (j == 0 || !isTileAt(i+1, j-1, layer)) { // If we are at the top or the cell above is empty
							al_draw_bitmap(getRoundingTile(type, 0, 2, tileMap), (i + 1)*tileSize, j*tileSize, 0);
						}
						if (j == height - 1 || !isTileAt(i+1, j+1, layer)) { //If we are at the bottom or the cell bellow is empty
							al_draw_bitmap(getRoundingTile(type, 0, 2, tileMap), (i + 1)*tileSize, (2*j + 1)*tileSize/2, 0);
					}	}

					// Check the top and bottom
					// Corner pieces should be handled here

					// Starting with the top
					if (j > 0 && !isTileAt(i, j-1, layer)) {
						if (i == 0 || !isTileAt(i-1, j-1, layer)) { // If we are at the far left or the cell to the left is empty
							al_draw_bitmap(getRoundingTile(type, 0, 1, tileMap), i*tileSize, (2*j - 1)*tileSize/2, 0);
						} else {
							al_draw_bitmap(getRoundingTile(type, 1, 2, tileMap), i*tileSize, (2*j - 1)*tileSize/2, 0);
						}
						if (i == width-1 || !isTileAt(i+1, j-1, layer)) { // If we are at the far right or the cell to the right is empty
							al_draw_bitmap(getRoundingTile(type, 0, 1, tileMap), (2*i + 1)*tileSize/2, (2*j - 1)*tileSize/2, 0);
						} else {
							al_draw_bitmap(getRoundingTile(type, 1, 1, tileMap), (2*i + 1)*tileSize/2, (2*j - 1)*tileSize/2, 0);
					}	}
					// Now the bottom
					if (j < height-1 && !isTileAt(i, j+1, layer)) {
						if (i == 0 || !isTileAt(i-1, j+1, layer)) { // If we are at the far left or the cell to the left is empty
							al_draw_bitmap(getRoundingTile(type, 0, 3, tileMap), i*tileSize, (j + 1)*tileSize, 0);
						} else {
							al_draw_bitmap(getRoundingTile(type, 1, 3, tileMap), i*tileSize, (j + 1)*tileSize, 0);
						}
						if (i == width - 1 || !isTileAt(i+1, j+1, layer)) { // If we are at the far right or the cell to the right is empty
							al_draw_bitmap(getRoundingTile(type, 0, 3, tileMap), (2*i + 1)*tileSize/2, (j + 1)*tileSize, 0);
						} else {
							al_draw_bitmap(getRoundingTile(type, 1, 4, tileMap), (2*i + 1)*tileSize/2, (j + 1)*tileSize, 0);
					}	}

					// Check 'free' corners
					// these should not hane any full cells directly adjacent (only diagonally adjacent)

					// Top Left
					if ( (i > 0 && j > 0) && !isTileAt(i-1, j-1, layer) && !isTileAt(i, j-1, layer) && !isTileAt(i-1, j, layer) ) {
						al_draw_bitmap(getRoundingTile(type, 2, 1, tileMap), (2*i - 1)*tileSize/2, (2*j - 1)*tileSize/2, 0);
					}
					// Top Right
					if ( (i < width-1 && j > 0) && !isTileAt(i+1, j-1, layer) && !isTileAt(i, j-1, layer) && !isTileAt(i+1, j, layer) ) {
						al_draw_bitmap(getRoundingTile(type, 2, 2, tileMap), (i + 1)*tileSize, (2*j - 1)*tileSize/2, 0);
					}
					//Bottom Right
					if ( (i < width-1 && j < height-1) && !isTileAt(i+1, j+1, layer) && !isTileAt(i, j+1, layer) && !isTileAt(i+1, j, layer) ) {
						al_draw_bitmap(getRoundingTile(type, 2, 3, tileMap), (i + 1)*tileSize, (j + 1)*tileSize, 0);
					}
					//Bottom Left
					if ( (i > 0 && j < height-1) && !isTileAt(i-1, j+1, layer) && !isTileAt(i, j+1, layer) && !isTileAt(i-1, j, layer) ) {
						al_draw_bitmap(getRoundingTile(type, 2, 4, tileMap), (2*i - 1)*tileSize/2, (j + 1)*tileSize, 0);
		}	}	}	}
		al_destroy_bitmap(tileMap);
		al_destroy_bitmap(fullTile);

	}

	else if (type == JIBUN_SINGLE_TILE) {
		ALLEGRO_BITMAP *tile;
		tile = al_load_bitmap(texturePath);
		if (!tile) {
			cout << "Couldn't find: " << texturePath << endl;
			return false;
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (isTileAt(i, j, layer)) {
					al_draw_bitmap(tile, i*tileSize, j*tileSize, 0);
		}	}	}

		al_destroy_bitmap(tile);
	}

	else if (type == JIBUN_SOLID_COLOR) {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (isTileAt(i, j, layer)) {
					al_draw_filled_rectangle(i*tileSize, j*tileSize, (i+1)*tileSize, (j+1)*tileSize, color);
		}	}	}

	}

	else {
		cout << "Invalid genTex type." << endl;
		return false;
	}
	return true;
}

bool Level::genTex() {
	tex = al_create_bitmap(width*tileSize, height*tileSize);
	if (!tex) {
		cout << "Failed to create level bitmap" << endl;
		return false;
	}
	al_set_target_bitmap(tex);
	al_clear_to_color(al_map_rgba(0,0,0,0));

	//This looks nicer doesn't it?
	if(!drawLayer(JIBUN_LADDER, tex)) {
		cout << "Initialisation failed on rendering ladders." << endl;
		return false;
	}
	if(!drawLayer(JIBUN_ONE_WAY_FLOOR, tex)) {
		cout << "Initialisation failed on rendering one way floors." << endl;	//TODO// Implement some draw layering system of some sort
		return false;
	}
	if(!drawLayer(JIBUN_SOLID, tex)) {
		cout << "Initialisation failed on rendering solid tiles" << endl;		//Important that solid tiles are last. (so they are layered on top)
		return false;
	}

	return true;
}

vector<vector<bool> > *Level::getLayerArray(int layer) {
	switch (layer) {
		case JIBUN_SOLID:
			return solidArray;
		case JIBUN_LADDER:
			return ladderArray;
		case JIBUN_ONE_WAY_FLOOR:
			return oneWayFloorArray;
		default:
			return NULL;
	}
}

//------------
// Behaviour
//------------
Level::Level(TextureWrapper* textures, ALLEGRO_COLOR color, unsigned int tileSize) {

	maskPath = textures->getSource(JIBUN_MASK);
	this->textures = textures;

	this->color = color;
	this->tileSize = tileSize;
}

bool Level::init() {
	// This creates a temporary bitmap of the file at path. It then creates a boolean "array" of these pixels storing true for a black pixel etc.
	ALLEGRO_BITMAP *mask = NULL;
	mask = al_load_bitmap(maskPath);
	if(!mask) {
		cout << "Couldn't find: " << maskPath << endl;
		return false;
	}
	if (!al_lock_bitmap(mask, al_get_bitmap_format(mask), ALLEGRO_LOCK_READONLY)) {
		cout << "Failed to lock mask" << endl;
		return false;
	}
	width = al_get_bitmap_width(mask);
	height = al_get_bitmap_height(mask);
	solidArray = new vector<vector<bool> >(width, vector<bool>(height)); // This looks confusing as hell, it just intialises the cell to a width by height "array".
	ladderArray = new vector<vector<bool> >(width, vector<bool>(height));
	oneWayFloorArray = new vector<vector<bool> >(width, vector<bool>(height));

	unsigned char *r, *g, *b;
	r = new unsigned char;
	g = new unsigned char;
	b = new unsigned char;

	for (int i = 0; i < width; i++) {
		for (int j =0; j < height; j++) {
			al_unmap_rgb(al_get_pixel(mask, i, j), r, g, b);
			setTileAt(i, j, *r == 0 && *g == 0 && *b == 0, JIBUN_SOLID);			// If pixel is black set solid to true in corresponding cell.
			setTileAt(i, j, *r == 0 && *g == 0 && *b == 255, JIBUN_LADDER);			// Likewise, if blue, it's a ladder
			setTileAt(i, j, *r == 0 && *g == 255 && *b == 0, JIBUN_ONE_WAY_FLOOR);	// Onajiku, if green, it's a one way floor.
	}	}

	delete r;
	delete g;
	delete b;

	al_destroy_bitmap(mask);
	if (!genTex()) {
		return false;
	}

	getManager()->setLevel(this);

	return true;
}

void Level::update(float delta) {
	//Do nothing...
}

void Level::draw(int dx, int dy, float scale) {
	al_draw_scaled_rotated_bitmap(tex, 0, 0, -dx, -dy, scale, scale, 0, 0);
}


//----------
// Setters
//----------
void Level::setTileAt(int x, int y, bool value, int layer) {
	if (x >= width || y >= height || x < 0 || y < 0) return;

	getLayerArray(layer)->at(x).at(y) = value;
}


//----------
// Getters
//----------
bool Level::isTileAt(int x, int y, int layer) {
	if (layer > JIBUN_ANTILAYER) { //invert output
		layer -= JIBUN_ANTILAYER;

		if (x >= width || y >= height || x < 0 || y < 0) return false;
		return !getLayerArray(layer)->at(x).at(y);
	}
	else if (layer == JIBUN_ANY) {
		return isTileAt(x, y) || isTileAt(x, y, JIBUN_LADDER) || isTileAt(x, y, JIBUN_ONE_WAY_FLOOR);
	}
	//else

	if (x >= width || y >= height || x < 0 || y < 0) return true;
	return getLayerArray(layer)->at(x).at(y);
}
